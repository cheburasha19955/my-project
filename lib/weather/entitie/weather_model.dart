import 'package:counter_on_elementary/weather/entitie/weather.dart';

class WeatherModel {
  final Weather currentWeather;
  final List<Weather> hourlyWeather;

  WeatherModel({required this.currentWeather, required this.hourlyWeather});
}

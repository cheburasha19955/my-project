import 'package:elementary/elementary.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../abstract_app_scope.dart';
import 'counter_screen.dart';
import 'counter_screen_model.dart';

abstract class ICounterWidgetModel extends IWidgetModel {}

CounterWM defaultCounterWidgetModelFactory(BuildContext context) {
  final appScope = context.read<IAppScope>();
  final model = CounterModel(appScope.errorHandler);
  return CounterWM(model);
}

class CounterWM extends WidgetModel<CounterScreen, CounterModel>
    implements ICounterWidgetModel {
  CounterWM(CounterModel model) : super(model);
}

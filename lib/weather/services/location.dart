import 'package:geolocator/geolocator.dart';

/// Определяем текущую позицию устройства.
///
/// Когда службы определения местоположения не включены или разрешения
/// отказано, `Future` вернет ошибку
class Location {
  Future<Position> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Проверяем, включены ли службы определения местоположения.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Службы определения местоположения не включены, не продолжайте
      // доступ к позиции и запрос пользователей
      // Приложение для включения служб определения местоположения.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Разрешения запрещены, в следующий раз вы можете попробовать
        // повторный запрос разрешений (здесь также
        // Android долженShowRequestPermissionRationale
        // вернули истину. В соответствии с рекомендациями Android
        // теперь ваше приложение должно отображать поясняющий пользовательский интерфейс.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Разрешения запрещены навсегда, обработайте соответствующим образом.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // Когда мы достигнем этого места, разрешения будут предоставлены, и мы сможем
    // продолжить доступ к позиции устройства.
    return await Geolocator.getCurrentPosition();
  }
}

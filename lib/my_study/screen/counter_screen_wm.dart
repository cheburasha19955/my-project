import 'package:elementary/elementary.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// import 'package:provider/provider.dart';
import 'package:flutter/widgets.dart';

import '../../weather/entitie/weather.dart';
import '../../weather/services/weather_service.dart';
import '../abstract_app_scope.dart';
import '../screen_two/screen_two.dart';
import 'counter_screen.dart';
import 'counter_screen_model.dart';

/// в абстрактный класс пишется то, что необходимо для функционирования виджета
/// параметры и методы
/// (переход на другую страницу, какая нибудь задержка, вызовы чего либо
/// в общем всего того что ты писал бы в виджете)

abstract class ICounterWidgetModel extends IWidgetModel {
  ListenableState<EntityState<int>> get counterState;
  String get myValue;

  // ValueNotifier get valueNotifier;

//сдесь может быть коментарий к тому что делает данный метод
// например - сохранение полученных значений

  void increment();

  void decrement();

  void back();
  void watchMyValue();

}

/// /////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
/// инициализация модели
/// инициализируются поля MODEL которые указаны в конструкторе
/// кроме аргументов ничего менять не надо
CounterWM defaultCounterWidgetModelFactory(BuildContext context) {
  // final appScope = context.read<IAppScope>();
  final model = CounterModel(
      // appScope.errorHandler,
      'условный сервис');

  return CounterWM(model);
}

/// ////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
/// инициализация полей и определение методов которые указаны в абстрактном классе
/// (в самом верху)
class CounterWM extends WidgetModel<CounterScreen, CounterModel>
    implements ICounterWidgetModel {
  CounterWM(CounterModel model) : super(model);

// все что выше менять не надо
  // можем задать первое значение сосояния
  final _counterState = EntityStateNotifier<int>( const EntityState(data: 0));

  int content = 0;
  @override
  String myValue ='';

  @override
  ListenableState<EntityState<int>> get counterState => _counterState;


  @override
  void increment() {
    // пример воздействия на текущее состояние content()
    _counterState.content((_counterState.value?.data ?? 0) + 1);
    content = _counterState.value?.data ?? content;
    if ((_counterState.value?.data ?? content) > 5) {
      //пример того как запустить одно из сосояний loading() или error()
      _counterState.loading();
    }
  }

  @override
  void decrement() {
    _counterState.content((_counterState.value?.data ?? 0) - 1);
    content = _counterState.value?.data ?? content;
    if ((_counterState.value?.data ?? content) < 0) {
      _counterState.error();
    }
  }

  @override
  void back() {
    Navigator.of(context).push(
      CupertinoPageRoute(
        builder: (context) =>
         TwoScreen(content: content),
      ),
    );
  }
  @override
  Future<void> watchMyValue()async{
    _counterState.content(content);
    myValue = '${widget.myValue}';
  }
}

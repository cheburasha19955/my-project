import 'package:elementary/elementary.dart';

/// //////// слой бизнес-логики ///////////////////////////////////////

/// сдесь должны быть:
/// какие то расчеты и изменения данных
/// сетевые запросы
/// доступ к
/// ... базе данных
/// ... геолокации
/// ... камере
/// ... хранилищу
/// ... и т.п.
class CounterModel extends ElementaryModel {
  CounterModel(ErrorHandler errorHandler) : super(errorHandler: errorHandler);
}

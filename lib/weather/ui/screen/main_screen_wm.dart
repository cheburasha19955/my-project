import 'package:counter_on_elementary/weather/entitie/weather.dart';
import 'package:counter_on_elementary/weather/ui/screen/main_screen.dart';
import 'package:counter_on_elementary/weather/ui/screen/widgets/custom_search_delegate.dart';
import 'package:dio/dio.dart';
import 'package:elementary/elementary.dart';
import 'package:flutter/material.dart';

import '../../entitie/weather_model.dart';
import '../../services/weather_service.dart';
import 'main_screen_model.dart';

abstract class IMainWidgetModel extends IWidgetModel {
  ListenableState<EntityState<WeatherModel>> get currentWeather;

  locationSearch();

  myPositionSearch();
}

MainWM defaultCounterWidgetModelFactory(BuildContext context) {
  final model = MainModel(WeatherService());
  return MainWM(model);
}

class MainWM extends WidgetModel<MainScreen, MainModel>
    implements IMainWidgetModel {
  MainWM(MainModel model) : super(model);

  final EntityStateNotifier<WeatherModel> _currentWeather =
      EntityStateNotifier(null);

  @override
  ListenableState<EntityState<WeatherModel>> get currentWeather =>
      _currentWeather;

  @override
  void initWidgetModel() {
    super.initWidgetModel();
    _loadWeather();
  }

  Future<void> _loadWeather({
    String text = 'Moscow',
    String lat = '',
    String lon = '',
  }) async {
    try {
      _currentWeather.loading();
      final weatherModel = await model.getCurrentWeather(
        lat: lat,
        lon: lon,
        text: text,
      );
      _currentWeather.content(weatherModel);
    } on DioError catch (err) {
      _currentWeather.error(err);
    }
  }

  @override
  void locationSearch() {
    showSearch(
      context: context,
      delegate: MySearchDelegate(
        (query) {
          _loadWeather(text: query);
        },
      ),
    );
  }

  @override
  Future<void> myPositionSearch() async {
    final position = await model.getPosition();
    _loadWeather(
      text: '',
      lon: '${position.longitude}',
      lat: '${position.latitude}',
    );
  }
}

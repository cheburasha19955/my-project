import 'package:counter_on_elementary/weather/entitie/weather.dart';
import 'package:counter_on_elementary/weather/ui/screen/main_screen_model.dart';
import 'package:counter_on_elementary/weather/ui/screen/main_screen_wm.dart';
import 'package:counter_on_elementary/weather/ui/screen/widgets/main_screen_wrapper.dart';
import 'package:elementary/elementary.dart';
import 'package:flutter/material.dart';

import '../../entitie/weather_model.dart';

class MainScreen extends ElementaryWidget<IMainWidgetModel> {
  const MainScreen({
    Key? key,
    WidgetModelFactory wmFactory = defaultCounterWidgetModelFactory,
  }) : super(wmFactory, key: key);

  @override
  Widget build(IMainWidgetModel wm) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: const Color.fromRGBO(0, 0, 0, 0),
        actions: [
          IconButton(
            icon: const Icon(Icons.my_location,color: Colors.black),
            onPressed: () =>wm.myPositionSearch(),
          ),
          IconButton(
            icon: const Icon(Icons.search,color: Colors.black),
            onPressed: () =>wm.locationSearch(),
          ),
        ],
      ),
      body: EntityStateNotifierBuilder<WeatherModel>(
        listenableEntityState: wm.currentWeather,
        loadingBuilder: (context, data) {
          return const Center(
            child:  CircularProgressIndicator(),
          );
        },
        errorBuilder: (context, _, __) {
          return const Text('Ошибка');
        },
        builder: (context, counterState) {
          return MainScreenWrapper(
            hourlyWeather: wm.currentWeather.value!.data!.hourlyWeather,
            weather: wm.currentWeather.value!.data!.currentWeather);
        },
      ),
    );
  }
}

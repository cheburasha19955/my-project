import 'package:flutter/material.dart';

class TwoScreen extends StatelessWidget {
  const TwoScreen({Key? key, required this.content}) : super(key: key);
final int content;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
             Text(
              '$content',
              style: TextStyle(fontSize: 20),
            ),
            ElevatedButton(
              child: const Text('Назад'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
    );
  }
}

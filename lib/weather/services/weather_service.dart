import 'dart:convert';

import 'package:dio/dio.dart';

import '../entitie/weather.dart';


class WeatherService {
  static String _apiKey = 'a95655c7e174233b4ac42985c8ab41fa';

  /// функция запроса текущей погоды с сервера
  // query - запрос
  // lat - широта
  // lon - долгота
   Future<Weather> fetchCurrentWeather(
      {required String query, String lat = '', String lon = ''}) async {
    var url =
        'http://api.openweathermap.org/data/2.5/weather?q=$query&lat=$lat&lon=$lon&appid=$_apiKey&units=metric';
    var dio = Dio();
    // POST позволяет клиенту добавить свои данные на сервер
    final response = await dio.post(url);
    // обрабатываем ответы с сервера
    // statusCode == 200 - все хорошо
    if (response.statusCode == 200) {
      /// непонятно !!!

      return Weather.fromJson(response.data);

    } else {
      throw Exception('Не удалось загрузить погоду');
    }
  }



// fetchHourlyWeather - получить почасовую погоду
   Future<List<Weather>> fetchHourlyWeather(
      {required String query, String lat = "", String lon = ""}) async {
    var url =
        'http://api.openweathermap.org/data/2.5/forecast?q=$query&lat=$lat&lon=$lon&appid=$_apiKey&units=metric';
    var dio = Dio();
    final response = await dio.post(url);
    if (response.statusCode == 200) {
      /// непонятно !!!
      final jsonData = response.data;
      final List<Weather> data =
          (jsonData['list'] as List<dynamic>).map((item) {
        return Weather.fromJsonList(item);
      }).toList();
      return data;
    } else {
      throw Exception('Failed to load weather');
    }
  }
}

/*
void myFunc(){
  WeatherService hh = WeatherService();
  hh.так не получится
  со static только так
  WeatherService.fetchCurrentWeather();
}
*/

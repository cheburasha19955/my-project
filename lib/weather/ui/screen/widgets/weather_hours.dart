import 'package:counter_on_elementary/weather/ui/screen/widgets/weather_card.dart';
import 'package:flutter/widgets.dart';

import '../../../entitie/weather.dart';

class WeatherHours extends StatelessWidget {
  final List<Weather> hourlyWeather;

  const WeatherHours({Key? key, required this.hourlyWeather}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: hourlyWeather.length,
      itemBuilder: (context, i) {
        return WeatherCard(
          title:
              '${hourlyWeather[i].time.hour}:${hourlyWeather[i].time.minute}0',
          temperature: hourlyWeather[i].temperature.toInt(),
          iconCode: hourlyWeather[i].iconCode,
          temperatureFontSize: 20,
        );
      },
    );
  }
}

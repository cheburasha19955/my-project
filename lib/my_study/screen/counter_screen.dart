
import 'package:elementary/elementary.dart';
import 'package:flutter/material.dart';

import 'counter_screen_wm.dart';


/// Только верстка
/// Если необходимо выполнить какую либо операцию
/// то эта операция (функция) должна быть расписана в виджет модели (WM)
/// а сдесь она только вызывается, НО НЕ ПИШЕТСЯ!
class CounterScreen extends ElementaryWidget<ICounterWidgetModel> {
  const CounterScreen(this.myValue, {
    Key? key,
    WidgetModelFactory wmFactory = defaultCounterWidgetModelFactory,
  }) : super(wmFactory, key: key);
final int myValue;
  @override
  Widget build(ICounterWidgetModel wm) {
    return Scaffold(
      body: Center(
        child: Container(
          color: Colors.amber,
          child: EntityStateNotifierBuilder<int>(
            listenableEntityState: wm.counterState,
            loadingBuilder: (context, data) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children:   [
                  const CircularProgressIndicator(),
                  const Text('Число больше пяти поэтому'),
                  const Text('Я в состоянии загрузки'),
                  Back(wm: wm),
                ],
              );
            },
            errorBuilder: (context, _, __) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children:  [
                  const Text('Число меньше 0'),
                  const Text('Я в состоянии ошибки'),
                  const Text(
                    'Errror!',
                    style: TextStyle(
                      color: Colors.red,
                      fontSize: 40,
                    ),
                  ),
                  Back(wm: wm),
                ],
              );
            },
            builder: (context, counterState) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('Значение добавленное при создании виджета'),
                  Text(wm.myValue),
                  const Text('Значение счетчика из стейта'),
                  Text('${wm.counterState.value?.data}'),
                  ElevatedButton(
                    child: const Text('Увеличить значение'),
                    onPressed: () {
                      wm.increment();
                    },
                  ),
                  ElevatedButton(
                    child: const Text('Уменьшить значение'),
                    onPressed: () {
                      wm.decrement();
                    },
                  ),
                  ElevatedButton(
                    child: const Text('Перейти на другую страницу'),
                    onPressed: () {
                      wm.back();
                    },
                  ),
                  ElevatedButton(
                    child: const Text('Показать значение из виджета'),
                    onPressed: () {
                      wm.watchMyValue();
                    },
                  ),

                  ///отрисовка виджета в зависимости от значения модели
                  EntityStateNotifierBuilder<int>(
                    listenableEntityState: wm.counterState,
                    builder: (context, int? counterState) {
                      return counterState == 3
                          ? Text('Допустим сеть есть')
                          : Text('Допустим сети нет');
                    },
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

class Back extends StatelessWidget {
   const Back({Key? key, required this.wm}) : super(key: key);
final  ICounterWidgetModel wm;
  @override
  Widget build(BuildContext context) {
    return                   ElevatedButton(
      child: const Text('Назад'),
      onPressed: () {
        wm.back();
      },
    );
  }
}


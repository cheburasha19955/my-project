import 'package:counter_on_elementary/weather/services/location.dart';
import 'package:elementary/elementary.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

import '../../entitie/weather.dart';
import '../../entitie/weather_model.dart';
import '../../services/weather_service.dart';

/// //////// слой бизнес-логики ///////////////////////////////////////

/// сдесь должны быть:
/// какие то расчеты и изменения данных
/// сетевые запросы
/// доступ к
/// ... базе данных
/// ... геолокации
/// ... камере
/// ... хранилищу
/// ... и т.п.
class MainModel extends ElementaryModel {
  final WeatherService _addressService;

  MainModel(this._addressService) : super();

  Future<WeatherModel> getCurrentWeather({
    required String text ,
    required String lat ,
    required String lon ,
  }) async {
    final currentWeather = await _addressService.fetchCurrentWeather(
      query: text,
      lat: lat,
      lon: lon,
    );
    final hourlyWeather = await _addressService.fetchHourlyWeather(
      query: text,
      lat: lat,
      lon: lon,
    );

    return WeatherModel(
      currentWeather: currentWeather,
      hourlyWeather: hourlyWeather,
    );
  }

  Future<Position> getPosition() async {
    Location location = Location();
    return await location.determinePosition();
  }
}


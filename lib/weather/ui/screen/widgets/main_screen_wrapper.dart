import 'package:counter_on_elementary/weather/entitie/weather.dart';
import 'package:counter_on_elementary/weather/ui/screen/widgets/weather_card.dart';
import 'package:counter_on_elementary/weather/ui/screen/widgets/weather_hours.dart';
import 'package:flutter/material.dart';

class MainScreenWrapper extends StatelessWidget {
 final Weather  weather;
  final List<Weather>  hourlyWeather;
  const MainScreenWrapper({
    Key? key, required this.weather, required this.hourlyWeather,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Text(
            weather.cityName,
            style: const TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
          ),
          Text(weather.description),
          const Spacer(),
          WeatherCard(
            title: 'Now',
            temperature: weather.temperature,
            iconCode: weather.iconCode,
            temperatureFontSize: 64,
            iconScale: 1,
          ),
          const Spacer(),
          Expanded(child: WeatherHours(hourlyWeather: hourlyWeather))
        ],
      ),
    );
  }
}

import 'package:elementary/elementary.dart';
import 'package:flutter/material.dart';

import 'counter_screen_wm.dart';

class CounterScreen extends ElementaryWidget<ICounterWidgetModel> {
  const CounterScreen({
    Key? key,
    WidgetModelFactory wmFactory = defaultCounterWidgetModelFactory,
  }) : super(wmFactory, key: key);

  @override
  Widget build(ICounterWidgetModel wm) {
    return Scaffold(
      body: EntityStateNotifierBuilder<int>(
        listenableEntityState: wm.counterState,
        loadingBuilder: (context, data) {
          return Text('Загрузка');
        },
        errorBuilder: (context, _, __) {
          return Text('Ошибка');
        },
        builder: (context, counterState) {
          return Text('Все ок');
        },
      ),
    );
  }
}
